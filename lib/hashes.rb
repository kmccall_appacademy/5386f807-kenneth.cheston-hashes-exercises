# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  str = str.split(" ")
  result_hash = Hash.new

  str.each do |word|
    result_hash[word] = word.length
  end
  result_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  array = hash.sort_by { |k, v| v }
  array[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k, v|
    older[k] = v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  word = word.split("")
  counter_hash = Hash.new(0)

  word.each do |letter|
    counter_hash[letter] += 1
  end
  counter_hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hash = Hash.new

  arr.each do |number|
    hash[number] = true
  end
  hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  hash = {odd: 0, even: 0}

  numbers.each do |number|
    hash[:odd] += 1 if number.odd?
    hash[:even] += 1 if number.even?
  end
  hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  string = string.split("")
  vowels = "aeiou"
  hash = Hash.new(0)

  string.each do |letter|
    next if vowels.include?(letter) == false
    hash[letter] += 1
  end

  array = hash.sort_by { |k, v| k }

  result = [0, 0]

  array.each do |el|
    result = el if el[1] > result[1]
  end
  result[0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  students.reject! {|k, v| v < 7}
  array = []
  students = students.to_a

  students.each_with_index do |el, idx|
    students[idx+1..-1].each do |el2|
      array << [el[0], el2[0]]
    end
  end
  array
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  counter_hash = Hash.new(0)

  specimens.each do |animal|
    counter_hash[animal] += 1
  end

  sorted_hash = counter_hash.sort_by {|k, v| k.length}

  number_of_species = counter_hash.keys.count
  smallest_population_size = sorted_hash[0][0].length
  largest_population_size = sorted_hash[-1][0].length
  #largest population size equals 22... which means the result for ["cat", "leopard-spotted ferret",
  # "dog"] should be 1.... not making sense to me... will look at solution after submittal
  number_of_species**2 * smallest_population_size / smallest_population_size

end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  # vandalized_sign = vandalized_sign.split
  v_hash = character_count(vandalized_sign)
  n_hash = character_count(normal_sign)

  vandalized_sign.split("").each do |letter|

    return false if v_hash[letter] > n_hash[letter] || n_hash[letter] == nil
  end
  true
end

def character_count(str)
  str = str.downcase.split("")
  counter_hash = Hash.new(0)

  str.each do |letter|
    counter_hash[letter] += 1
  end
  counter_hash
end
